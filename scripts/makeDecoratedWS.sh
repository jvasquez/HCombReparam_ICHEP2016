# DS2.1 + DS2 (Final?)
fileHZZ="/afs/cern.ch/atlas/project/HSG7/Run2/ICHEP2016/HZZ/08-02-2016/H4l_STXS_13TeV.root"
fileHGam="/afs/cern.ch/atlas/project/HSG7/Run2/ICHEP2016/HGam/07-29-2016/WS-HGam-HTXS.root"

dir="HComb_ICHEP2016/workspaces/decorated_input"

manager -w decorate -f $fileHZZ -p $dir/WS-HZZ-Decorated.root
manager -w decorate -f $fileHGam -p $dir/WS-HGam-Decorated.root -d combData
