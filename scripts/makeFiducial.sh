#fileHGam="/afs/cern.ch/atlas/project/HSG7/Run2/ICHEP2016/Fiducial/HGam/07-31-2016/HGamXSCombWS_July31_prw2.root"
fileHGam="/afs/cern.ch/atlas/project/HSG7/Run2/ICHEP2016/Fiducial/HGam/08-03-2016/HGamXSCombWS_Aug3_FullMassRange_SplitPESPER.root"
fileHZZ="/afs/cern.ch/atlas/project/HSG7/Run2/ICHEP2016/Fiducial/HZZ/07-31-2016/fixed_sys.root"

dir="HComb_ICHEP2016/workspaces/fiducial"

manager -w decorate -f $fileHGam -p $dir/WS-HGam-Fiducial.root -d dataset
manager -w decorate -f $fileHZZ -p $dir/WS-HZZ-Fiducial.root
#manager -w combine -x HComb_ICHEP2016/cfg/fiducial/onlyHGam.xml -f HComb_ICHEP2016/workspaces/fiducial/WS-Comb-OnlyHGam-Fiducial.root
#manager -w combine -x HComb_ICHEP2016/cfg/fiducial/onlyHZZ.xml -f HComb_ICHEP2016/workspaces/fiducial/WS-Comb-OnlyHZZ-Fiducial.root
manager -w combine -x HComb_ICHEP2016/cfg/fiducial/combination.xml -f HComb_ICHEP2016/workspaces/fiducial/WS-Comb-Fiducial.root 
