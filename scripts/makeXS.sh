manager -w organize -x HComb_ICHEP2016/cfg/model_XS/reparameterize_yy.xml
manager -w organize -x HComb_ICHEP2016/cfg/model_XS/reparameterize_ZZ.xml
manager -w combine -x HComb_ICHEP2016/cfg/model_XS/onlyHGam.xml -f HComb_ICHEP2016/workspaces/model_XS/WS-Comb-XS-OnlyHGam.root -s 0 -t -1 
manager -w combine -x HComb_ICHEP2016/cfg/model_XS/onlyHZZ.xml -f HComb_ICHEP2016/workspaces/model_XS/WS-Comb-XS-HZZ-OnlyHZZ.root -s 0 -t -1 
manager -w combine -x HComb_ICHEP2016/cfg/model_XS/combination.xml -f HComb_ICHEP2016/workspaces/model_XS/WS-Comb-XS.root -s 0 -t -1

# Run NP Check on Combined WS
cmd="root -q -b -l HComb_ICHEP2016/macros/testNPs.C(\"HComb_ICHEP2016/workspaces/model_XS/WS-Comb-XS.root\")"
$cmd
