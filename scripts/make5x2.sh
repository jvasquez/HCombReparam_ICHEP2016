manager -w organize -x HComb_ICHEP2016/cfg/model_5x2/reparameterize_yy.xml
manager -w organize -x HComb_ICHEP2016/cfg/model_5x2/reparameterize_ZZ.xml
manager -w combine -x HComb_ICHEP2016/cfg/model_5x2/onlyHGam.xml -f HComb_ICHEP2016/workspaces/model_5x2/WS-Comb-5x2-OnlyHGam.root -s 0 -t -1 
manager -w combine -x HComb_ICHEP2016/cfg/model_5x2/onlyHZZ.xml -f HComb_ICHEP2016/workspaces/model_5x2/WS-Comb-5x2-OnlyHZZ.root -s 0 -t -1 
manager -w combine -x HComb_ICHEP2016/cfg/model_5x2/combination.xml -f HComb_ICHEP2016/workspaces/model_5x2/WS-Comb-5x2.root -s 0 -t -1 

# Run NP Check on Combined WS
cmd="root -q -b -l HComb_ICHEP2016/macros/testNPs.C(\"HComb_ICHEP2016/workspaces/model_5x2/WS-Comb-5x2.root\")"
$cmd
