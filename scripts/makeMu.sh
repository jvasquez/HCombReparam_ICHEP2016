manager -w organize -x HComb_ICHEP2016/cfg/model_muLegacy_txs/add_theory_uncert_yy.xml
manager -w organize -x HComb_ICHEP2016/cfg/model_muLegacy_txs/add_theory_uncert_ZZ.xml
manager -w combine -x HComb_ICHEP2016/cfg/model_muLegacy_txs/onlyHGam.xml -f HComb_ICHEP2016/workspaces/model_muLegacy_txs/WS-Comb-muLegacy-OnlyHGam.root -s 0 -t -1 
manager -w combine -x HComb_ICHEP2016/cfg/model_muLegacy_txs/onlyHZZ.xml -f HComb_ICHEP2016/workspaces/model_muLegacy_txs/WS-Comb-muLegacy-OnlyHZZ.root -s 0 -t -1 
manager -w combine -x HComb_ICHEP2016/cfg/model_muLegacy_txs/combination.xml -f HComb_ICHEP2016/workspaces/model_muLegacy_txs/WS-Comb-muLegacy.root -s 0 -t -1

# Run NP Check on Combined WS
cmd="root -q -b -l HComb_ICHEP2016/macros/testNPs.C(\"HComb_ICHEP2016/workspaces/model_muLegacy_txs/WS-Comb-muLegacy.root\")"
$cmd
