manager -w organize -x HComb_ICHEP2016/cfg/model_XSBR/reparameterize_yy.xml
manager -w organize -x HComb_ICHEP2016/cfg/model_XSBR/reparameterize_ZZ.xml
manager -w combine -x HComb_ICHEP2016/cfg/model_XSBR/onlyHGam.xml -f HComb_ICHEP2016/workspaces/model_XSBR/WS-Comb-XSBR-OnlyHGam.root -s 0 -t -1 
manager -w combine -x HComb_ICHEP2016/cfg/model_XSBR/onlyHZZ.xml -f HComb_ICHEP2016/workspaces/model_XSBR/WS-Comb-XSBR-OnlyHZZ.root -s 0 -t -1 
manager -w combine -x HComb_ICHEP2016/cfg/model_XSBR/combination.xml -f HComb_ICHEP2016/workspaces/model_XSBR/WS-Comb-XSBR.root -s 0 -t -1 

# Run NP Check on Combined WS
cmd="root -q -b -l HComb_ICHEP2016/macros/testNPs.C(\"HComb_ICHEP2016/workspaces/model_XSBR/WS-Comb-XSBR.root\")"
$cmd
