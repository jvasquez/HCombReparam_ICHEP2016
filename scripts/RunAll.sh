./makeDecoratedWS.sh
sleep 10
echo "Producing Models in Background, check logs.."
nohup ./makeXSBR.sh &> log_XSBR.txt &
nohup ./makeXS.sh &> log_XS.txt &
nohup ./makeMu.sh &> log_Mu.txt &
nohup ./make5x2.sh &> log_5x2.txt &
echo "run cleanup when done"
