Configuration files and output workspaces to be run with the workspaceCombiner tool.

Installation
```
git clone https://:@gitlab.cern.ch:8443/yanght/workspaceCombiner.git
cd workspaceCombiner
git clone https://:@gitlab.cern.ch:8443/jvasquez/HCombReparam_ICHEP2016.git HComb_ICHEP2016
source setup.sh
make
```

Bash scripts to prepare full re-parameterization and combination also provided:
```
ln -s HComb_ICHEP2016/scripts/* .
. RunAll.sh
```
